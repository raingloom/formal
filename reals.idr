import Data.PosNat
import Data.ZZ

{-
win idris --nocolor -p contrib $%
-}

data Rational : Type where
	(/) : ZZ -> PosNat -> Rational

Semigroup Rational where
	(an/ad) <+> (bn/bd) = (an+bn) / (ad `plusPosNat` bd)

NZRational : Type
NZRational = DPair Rational (\(a/b)=>Not (a=0))
