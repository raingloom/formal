%default total

interface Groupoid g where
	(<+>) : g -> g -> g

IsLeftUnit : Groupoid g => g -> Type
IsLeftUnit s = (e : g) -> (s <+> e = e)

IsRightUnit : Groupoid g => g -> Type
IsRightUnit s = (e : g) -> (e <+> s = e)

IsUnit : Groupoid g => g ->Type
IsUnit s = (IsRightUnit s, IsLeftUnit s)

[GNatWithAdd] Groupoid Nat where
	(<+>) = (+)

interface Groupoid g => Semigroup g where
	isAssociative : (a,b,c : g) -> (a <+> (b <+> c)) = ((a <+> b) <+> c)

ZeroIsRightUnit : IsRightUnit @{GNatWithAdd} Z
ZeroIsRightUnit = plusZeroRightNeutral

ZeroIsLeftUnit : IsLeftUnit @{GNatWithAdd} Z
ZeroIsLeftUnit = plusZeroLeftNeutral

ZeroIsUnit : IsUnit @{GNatWithAdd} Z
ZeroIsUnit = (ZeroIsRightUnit, ZeroIsLeftUnit)

[SGNatWithAdd] Main.Semigroup Nat using GNatWithAdd where
	isAssociative = plusAssociative

interface Main.Semigroup g => UnitSemigroup g where
	hasUnit : DPair g $ \s => IsUnit s

unit : UnitSemigroup g => g
unit = fst hasUnit

inversePair : UnitSemigroup g => (g,g) -> Type
inversePair (a,b) = a <+> b = unit

interface UnitSemigroup g => Group g where
	invertible : (a : g) -> DPair g $ \b => inversePair (a,b)

invert : Group g => g -> g
invert e = fst $ invertible e

commutative : (t : Type) -> (t -> t -> t) -> Type
commutative t f = (a,b : t) -> (f a b = f b a)

interface Group g => Abelian g where
	isCommutative : commutative g (<+>)

homomorphism : Groupoid g => Groupoid g' => (g -> g') -> Type
homomorphism h {g} = (a,b : g) -> (a <+> b) = (h a <+> h b)

injective : (t -> t') -> Type
injective f {t} = (a,b : t) -> (f a = f b) -> (a = b)

monomorphism : Groupoid g => Groupoid g' => (f : g -> g') -> {homo : homomorphism f} -> Type
monomorphism f = injective f

surjective : (t -> t') -> Type
surjective f {t} {t'} = (b : t') -> DPair t $ \a => f a = b

epimorhpism : Groupoid g => Groupoid g' => (f : g -> g') -> {homo : homomorphism f} -> Type
epimorhpism f = surjective f

bijective : (t -> t') -> Type
bijective f {t} {t'} = (a,b : t) -> (a',b' : t') -> a' = b' -> f a = f b

isomorphism : Groupoid g => Groupoid g' => (f : g -> g') -> {homo : homomorphism f} -> Type
isomorphism f = bijective f

endomorphism : Groupoid g => (g -> g) -> Type
endomorphism h {g} = (a,b : g) -> (a <+> b) = (h a <+> h b)

automorphism : Groupoid g => (f : g -> g) -> {endo : endomorphism f} -> Type
automorphism f = bijective f

homoHomoStillHomo : 
	Groupoid g => Groupoid g' => Groupoid g'' =>
	(h0 : g -> g') -> (h1 : g' -> g'') ->
	(homomorphism h0) -> (homomorphism h1) -> (homomorphism (h1 . h0))
homoHomoStillHomo = ?homoHomoStillHomo_rhs

GroupoidToDPair : Groupoid g => DPair Type $ \g=>(g -> g -> g)
GroupoidToDPair {g} = (g ** (<+>))

infixr 9 ^
(^) : Group g => g -> Nat -> g
(^) e Z = unit
(^) e (S n) = e <+> (e ^ n)

finRank : Group g => g -> Nat -> Type
finRank e n = e ^ n = unit
