import Data.Vect

data NT n t = N n | T t

data ChomskyRule n t = Aa n t | ABC n n n

Chomsky : Type -> Type -> Type
Chomsky n t = List $ ChomskyRule n t

NestedVect : Nat -> Type -> Type
NestedVect k t = Vect k $ DPair Nat $ \j=>Vect j t

data Pyramid : Nat -> (t : Type) -> Type where
	PyramidBase : Pyramid Z t
	PyramidSucc : Vect (S m) t -> Pyramid m t -> Pyramid (S m) t

CYKPyramid : Nat -> Type -> Type
CYKPyramid h n = Pyramid h n

take : (n : Nat) -> Stream t -> Vect n t
take Z _ = Nil
take (S n) (x::xs) = x :: take n xs

cykPyramid0 : (h : Nat) -> {n : Type} -> CYKPyramid h (List n)
cykPyramid0 Z = PyramidBase
cykPyramid0 (S h) = PyramidSucc (take (S h) $ repeat Nil) $ cykPyramid0 h

{-
cykPyramid0FromWord : Vect l t -> {n : Type} -> cykPyramid l n
cykPyramid0FromWord w =
	let pyramid = cykPyramid0 $ length w in
-}

cykFindMatchingAa : Eq n => Eq t => Chomsky n t -> t -> List n
cykFindMatchingAa rules char = go rules where
	go : Chomsky n t -> List n
	go Nil = Nil
	go ((Aa nterm term)::rs) = if term == char then nterm::go rs else go rs
	go (r::rs) = go rs

cykInitialRow : Eq n => Eq t => Chomsky n t -> List t -> List $ List n
cykInitialRow rules word = map (cykFindMatchingAa rules) word

data Nonterminals n = MKN n
data Terminals t = MKT t

Eq a => Eq (Nonterminals a) where
	(MKN x) == (MKN y) = x == y

Eq a => Eq (Terminals a) where
	(MKT x) == (MKT y) = x == y

testGrammar : Chomsky (Nonterminals Char) (Terminals Char)
testGrammar =
	let ns = (MKN 'S') in
	let na = (MKN 'A') in
	let nb = (MKN 'B') in
	let a = (MKT 'a') in
	let b = (MKT 'b') in
	[ABC ns na nb
	,Aa na a
	,Aa nb b
	]
