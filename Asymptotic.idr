Real : Type
Real = Double

{-
win idris --nocolor $%
-}

RealSeries : Type
RealSeries = (Nat -> Real)

FnClass : Type
FnClass = (RealSeries -> RealSeries -> Type)

MkFnClass : (Real -> Real -> Bool) -> FnClass
MkFnClass cmp = \g => \f : RealSeries => DPair Real (\c=> (c > 0 = True) -> (n0,n : Nat) -> (n >= n0 = True) -> (f n `cmp` c*g n = True))

BigO : FnClass
BigO = MkFnClass (<=)

LilO : FnClass
LilO = MkFnClass (<)

BigOmega : FnClass
BigOmega = MkFnClass (>=)

LilOmega : FnClass
LilOmega = MkFnClass (>)

Theta : FnClass
Theta = \g, f => (BigO g f, BigOmega g f)

hanoiCost : RealSeries
hanoiCost n = pow 2 n - 1

hanoiIsExponential : BigO (pow 2) Main.hanoiCost
hanoiIsExponential = ?hanoi_prf